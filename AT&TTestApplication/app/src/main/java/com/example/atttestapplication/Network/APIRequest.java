package com.example.atttestapplication.Network;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.atttestapplication.AppController;
import com.example.atttestapplication.R;

import java.util.HashMap;
import java.util.Map;



public class  APIRequest {

    public static String TAG ="APIRequest";
    public static int MY_SOCKET_TIMEOUT_MS = 30000;
    public static void POST(final String url, final Map<String, String> params, final AppCallback callback     ){
        Log.w("POST URL",url);
       // Log.w("POST URL",params.toString());
        if (AppStatus.getInstance().isOnline())  {

            final RequestQueue queue = VolleySingleton.getInstance().getRequestQueue();
            StringRequest myReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String results) {
                    callback.onSuccess(results);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    callback.onError(volleyError.toString());
                    Toast.makeText(AppController.getContext(),volleyError.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            }) {
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

            };

            myReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(myReq);
        }
        else {
            Toast.makeText(AppController.getContext(),AppController.getContext().getString(R.string.no_internet_connection),
                    Toast.LENGTH_SHORT).show();
        }

    }
    public static void GET(final String url, final AppCallback callback){



         Log.w("GET URL",url);
        final RequestQueue queue = VolleySingleton.getInstance().getRequestQueue();
        StringRequest myReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String results) {
                //Log.w(TAG,results);


                callback.onSuccess(results);

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                callback.onError(volleyError.getMessage());
                Toast.makeText(AppController.getContext(),volleyError.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
        ) {

        };
        Map headers = new HashMap();

        myReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);

    }
}






