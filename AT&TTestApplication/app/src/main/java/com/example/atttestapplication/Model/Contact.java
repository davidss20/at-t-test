package com.example.atttestapplication.Model;

public class Contact
{
    private String nat;

    private String gender;

    private String phone;


    private Name name;


    private Location location;



    private String cell;

    private String email;

    private Picture picture;

    public String getNat ()
    {
        return nat;
    }

    public void setNat (String nat)
    {
        this.nat = nat;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }



    public Name getName ()
    {
        return name;
    }

    public void setName (Name name)
    {
        this.name = name;
    }



    public Location getLocation ()
    {
        return location;
    }

    public void setLocation (Location location)
    {
        this.location = location;
    }






    public String getCell ()
    {
        return cell;
    }

    public void setCell (String cell)
    {
        this.cell = cell;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public Picture getPicture ()
    {
        return picture;
    }

    public void setPicture (Picture picture)
    {
        this.picture = picture;
    }


    @Override
    public String toString() {
        return "Contact{" +
                "nat='" + nat + '\'' +
                ", gender='" + gender + '\'' +
                ", phone='" + phone + '\'' +
                ", name=" + name +
                ", location=" + location +
                ", cell='" + cell + '\'' +
                ", email='" + email + '\'' +
                ", picture=" + picture +
                '}';
    }
}