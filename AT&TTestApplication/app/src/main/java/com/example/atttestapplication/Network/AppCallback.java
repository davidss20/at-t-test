package com.example.atttestapplication.Network;

/**
 * Created by david-qeezz on 15/10/2015.
 */
public abstract class AppCallback {
    abstract public void onSuccess(String response);

    public void onError(String error) {

    }
    public void onMessage(String message) {

    }
}
