package com.example.atttestapplication.View;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
 import com.example.atttestapplication.Helpers.DownloadImageTask;
import com.example.atttestapplication.Model.Contact;
import com.example.atttestapplication.R;

public class ContactRecyclerViewAdapter extends RecyclerView.Adapter<ContactRecyclerViewAdapter.ViewHolder> {

    private Contact [] contacts;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Activity activity;

    // data is passed into the constructor
    public ContactRecyclerViewAdapter(Activity activity, Contact[] data) {
        this.mInflater = LayoutInflater.from(activity);
        this.contacts = data;
        this.activity=activity;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_contact, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact contact= contacts[position];
        holder.nameTextView.setText(contact.getName().getFirst() +" "+contact.getName().getLast());
        holder.numberTextView.setText(contact.getPhone());



            new DownloadImageTask( holder.contactImageView)
                    .execute(contact.getPicture().getMedium());




    }

    // total number of rows
    @Override
    public int getItemCount() {
        return contacts.length ;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nameTextView;
        TextView numberTextView;
        ImageView contactImageView;
        ViewHolder(View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.name);
            numberTextView= itemView.findViewById(R.id.phone_number);
            contactImageView= itemView.findViewById(R.id.image_contact);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showContact(activity ,contacts[getAdapterPosition()]);
                 }
            });
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    public   void showContact(Activity activity,Contact contact) {


        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_contact);

        TextView  nameContact    = (TextView) dialog.findViewById(R.id.name);
        TextView  emailContact   = (TextView) dialog.findViewById(R.id.email);
        TextView  addressContact = (TextView) dialog.findViewById(R.id.address);
        ImageView imageContact    = (ImageView) dialog.findViewById(R.id.image_contact);
        View closeBtn  = (View) dialog.findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        nameContact.setText(contact.getName().getFirst() +" "+contact.getName().getLast());
        emailContact.setText(contact.getEmail());
        addressContact.setText(contact.getLocation().getStreet()+" "+contact.getLocation().getState()+"  "+ contact.getLocation().getCity());
        new DownloadImageTask(imageContact)
                .execute(contact.getPicture().getLarge());
        // text.setText(msg);

        // Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
//        dialogButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });

        dialog.show();

    }
}