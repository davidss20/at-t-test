package com.example.atttestapplication.View;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.atttestapplication.R;

public class SplashActivity extends AppCompatActivity {
    private Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       getSupportActionBar().hide();
        ImageView image=(ImageView) findViewById(R.id.imageViewLogo);
        image.setAnimation(AnimationUtils.loadAnimation(this,R.anim.zoom_out));

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                 endSplash();
            }
        }, 3000);
    }

    private void endSplash() {
        Intent intent = new Intent(getApplicationContext(),
                MainActivity.class);
        startActivity(intent);
        finish();

    }
}
