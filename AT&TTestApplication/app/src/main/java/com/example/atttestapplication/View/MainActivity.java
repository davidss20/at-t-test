package com.example.atttestapplication.View;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;

import com.example.atttestapplication.Helpers.ConstHelper;
import com.example.atttestapplication.Model.Contact;
import com.example.atttestapplication.Network.APIRequest;
import com.example.atttestapplication.Network.AppCallback;
import com.example.atttestapplication.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity   extends AppCompatActivity   implements SwipeRefreshLayout.OnRefreshListener   {
    ContactRecyclerViewAdapter adapter;
    Contact[] contactsList= new Contact[0];
    Activity activity;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();
    }

    private void initView() {
        activity=this;


          recyclerView = findViewById(R.id.rv_contacts);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

        mSwipeRefreshLayout.setOnRefreshListener(this);
//        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                if(mSwipeRefreshLayout != null) {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
                // TODO Fetching data from server
                initData();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this  ));
        adapter = new ContactRecyclerViewAdapter(activity, contactsList  );

        recyclerView.setAdapter(adapter);
    }

    private void initData() {
        APIRequest.GET(ConstHelper.URL, new AppCallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject   jsonObject = new JSONObject(response);
                    String    contacts  = jsonObject.getString("results");
                    Gson gson = new Gson();
                    contactsList = gson.fromJson(contacts, Contact[].class);
                    adapter = new ContactRecyclerViewAdapter(activity , contactsList  );
                    recyclerView.setAdapter(adapter);
                    mSwipeRefreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    @Override
    public void onRefresh() {
        initData();
    }
}
